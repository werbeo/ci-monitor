package org.infinitenature.cimonitor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CiMonitorApplication {

	public static void main(String[] args) {
		SpringApplication.run(CiMonitorApplication.class, args);
	}
}
