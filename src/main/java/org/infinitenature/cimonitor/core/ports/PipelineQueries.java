package org.infinitenature.cimonitor.core.ports;

import java.util.stream.Stream;

import org.infinitenature.cimonitor.core.model.Pipeline;

public interface PipelineQueries {
	public Stream<Pipeline> findPipelines(int offset, int limit);

	public int count();
}
