package org.infinitenature.cimonitor.core.ports.sec;

import org.infinitenature.cimonitor.core.model.Pipeline;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class PipelinePersistenceQueriesImpl implements PipelinePersistenceQueries {
	private static final Logger LOGGER = LoggerFactory.getLogger(PipelinePersistenceQueries.class);

	@Override
	public Pipeline load(int id) {
		LOGGER.info("load: {}", id);
		return null;
	}

}
