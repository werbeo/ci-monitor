package org.infinitenature.cimonitor.core.ports.sec;

import org.infinitenature.cimonitor.core.model.Pipeline;

public interface PipelinePersistenceQueries {

	Pipeline load(int id);

}
