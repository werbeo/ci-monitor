package org.infinitenature.cimonitor.core.ports.sec;


import org.infinitenature.cimonitor.core.model.Pipeline;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class PipelinePersinstenceCommandsImpl implements PipelinePersistenceCommands{
private static final Logger LOGGER=LoggerFactory.getLogger(PipelinePersistenceCommands.class);
	@Override
	public void persist(Pipeline pipeline) {
		LOGGER.info("persist: {}",pipeline);
	}

}
