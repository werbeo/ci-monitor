package org.infinitenature.cimonitor.core.ports;

import org.infinitenature.cimonitor.core.model.Pipeline;

public interface PipelineCommands {
	public void createOrUpdatePipeline(Pipeline pipeline);

	public void setSourcePipeline(int pipelineId, int sourcePipelineId);

	public void linkPipeline(int parentId, int childId);
}
