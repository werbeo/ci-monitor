package org.infinitenature.cimonitor.core.model;

import java.util.Date;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class Build {

	private Integer id;
	private String stage;
	private String name;
	private String status;
	private Date createdAt;
	private Date startedAt;
	private Date finishedAt;
	private String when;
	private Boolean manual;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getStartedAt() {
		return startedAt;
	}

	public void setStartedAt(Date startedAt) {
		this.startedAt = startedAt;
	}

	public Date getFinishedAt() {
		return finishedAt;
	}

	public void setFinishedAt(Date finishedAt) {
		this.finishedAt = finishedAt;
	}

	public String getWhen() {
		return when;
	}

	public void setWhen(String when) {
		this.when = when;
	}

	public Boolean getManual() {
		return manual;
	}

	public void setManual(Boolean manual) {
		this.manual = manual;
	}

	@Override
	public String toString() {
		return BuildBeanUtil.doToString(this);
	}

	@Override
	public int hashCode() {
		return BuildBeanUtil.doToHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return BuildBeanUtil.doEquals(this, obj);
	}
}
