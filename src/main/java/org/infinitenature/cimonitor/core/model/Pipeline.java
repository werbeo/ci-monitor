package org.infinitenature.cimonitor.core.model;

import java.util.Date;
import java.util.List;

import org.infinitenature.cimonitor.core.model.PipelineBeanUtil;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class Pipeline {

	private Integer id;
	private String ref;
	private Boolean tag;
	private String sha;
	private String beforeSha;
	private String status;
	private List<String> stages;
	private Date createdAt;
	private Date finishedAt;
	private Integer duration;
	private Project project;
	private List<Build> builds;
	private Integer sourcePipelineId;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public Boolean getTag() {
		return tag;
	}

	public void setTag(Boolean tag) {
		this.tag = tag;
	}

	public String getSha() {
		return sha;
	}

	public void setSha(String sha) {
		this.sha = sha;
	}

	public String getBeforeSha() {
		return beforeSha;
	}

	public void setBeforeSha(String beforeSha) {
		this.beforeSha = beforeSha;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<String> getStages() {
		return stages;
	}

	public void setStages(List<String> stages) {
		this.stages = stages;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getFinishedAt() {
		return finishedAt;
	}

	public void setFinishedAt(Date finishedAt) {
		this.finishedAt = finishedAt;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public List<Build> getBuilds() {
		return builds;
	}

	public void setBuilds(List<Build> builds) {
		this.builds = builds;
	}

	public Integer getSourcePipelineId() {
		return sourcePipelineId;
	}

	public void setSourcePipelineId(Integer sourcePipelineId) {
		this.sourcePipelineId = sourcePipelineId;
	}

	@Override
	public String toString() {
		return PipelineBeanUtil.doToString(this);
	}

	@Override
	public int hashCode() {
		return PipelineBeanUtil.doToHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return PipelineBeanUtil.doEquals(this, obj);
	}
}
