package org.infinitenature.cimonitor.core.impl;

import org.infinitenature.cimonitor.core.model.Pipeline;
import org.infinitenature.cimonitor.core.ports.PipelineCommands;
import org.infinitenature.cimonitor.core.ports.sec.PipelinePersistenceCommands;
import org.infinitenature.cimonitor.core.ports.sec.PipelinePersistenceQueries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PipelineService implements PipelineCommands {
	@Autowired
	private PipelinePersistenceCommands persistenceCommands;
	@Autowired
	private PipelinePersistenceQueries persistenceQueries;

	@Override
	@Transactional
	public void createOrUpdatePipeline(Pipeline pipeline) {
		if (pipeline.getId() != null) {
			update(pipeline);
		} else {
			save(pipeline);
		}
	}

	private void save(Pipeline pipeline) {
		persistenceCommands.persist(pipeline);

	}

	private void update(Pipeline pipeline) {
		Pipeline persisted = persistenceQueries.load(pipeline.getId());
		merge(pipeline, persisted);
		persistenceCommands.persist(pipeline);

	}

	private void merge(Pipeline pipeline, Pipeline persisted) {
		pipeline.setSourcePipelineId(persisted.getSourcePipelineId());
	}

	@Override
	@Transactional
	public void setSourcePipeline(int pipelineId, int sourcePipelineId) {
		Pipeline pipeline = persistenceQueries.load(pipelineId);
		if (pipeline == null) {
			pipeline = new Pipeline();
			pipeline.setId(pipelineId);
		}
		pipeline.setSourcePipelineId(sourcePipelineId);
		persistenceCommands.persist(pipeline);
		;
	}

	@Override
	public void linkPipeline(int parentId, int childId) {
		// TODO Auto-generated method stub

	}
}
