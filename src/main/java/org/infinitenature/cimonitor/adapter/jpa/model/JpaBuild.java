package org.infinitenature.cimonitor.adapter.jpa.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import net.vergien.beanautoutils.annotation.Bean;

@Entity
@Table(name="build")
@Bean
public class JpaBuild {
	@Id
	private Integer id;
	private String stage;
	private String name;
	private String status;
	private Date createdAt;
	private Date startedAt;
	private Date finishedAt;
	private String when;
	private Boolean manual;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getStartedAt() {
		return startedAt;
	}

	public void setStartedAt(Date startedAt) {
		this.startedAt = startedAt;
	}

	public Date getFinishedAt() {
		return finishedAt;
	}

	public void setFinishedAt(Date finishedAt) {
		this.finishedAt = finishedAt;
	}

	public String getWhen() {
		return when;
	}

	public void setWhen(String when) {
		this.when = when;
	}

	public Boolean getManual() {
		return manual;
	}

	public void setManual(Boolean manual) {
		this.manual = manual;
	}

	@Override
	public String toString() {
		return JpaBuildBeanUtil.doToString(this);
	}

	@Override
	public int hashCode() {
		return JpaBuildBeanUtil.doToHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return JpaBuildBeanUtil.doEquals(this, obj);
	}
}
