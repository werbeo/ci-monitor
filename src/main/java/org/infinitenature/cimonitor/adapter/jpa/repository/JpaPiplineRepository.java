package org.infinitenature.cimonitor.adapter.jpa.repository;

import org.infinitenature.cimonitor.adapter.jpa.model.JpaPipeline;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaPiplineRepository extends JpaRepository<JpaPipeline, Integer> {

}
