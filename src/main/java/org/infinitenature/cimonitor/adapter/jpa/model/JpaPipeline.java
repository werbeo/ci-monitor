package org.infinitenature.cimonitor.adapter.jpa.model;

import java.util.Date;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import net.vergien.beanautoutils.annotation.Bean;

@Entity
@Table(name="pipeline")
@Bean
public class JpaPipeline {

	@Id
	private Integer id;
	private String ref;
	private Boolean tag;
	private String sha;
	private String beforeSha;
	private String status;
	@ElementCollection
	private List<String> stages;
	private Date createdAt;
	private Date finishedAt;
	private Integer duration;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public Boolean getTag() {
		return tag;
	}

	public void setTag(Boolean tag) {
		this.tag = tag;
	}

	public String getSha() {
		return sha;
	}

	public void setSha(String sha) {
		this.sha = sha;
	}

	public String getBeforeSha() {
		return beforeSha;
	}

	public void setBeforeSha(String beforeSha) {
		this.beforeSha = beforeSha;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<String> getStages() {
		return stages;
	}

	public void setStages(List<String> stages) {
		this.stages = stages;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getFinishedAt() {
		return finishedAt;
	}

	public void setFinishedAt(Date finishedAt) {
		this.finishedAt = finishedAt;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	@Override
	public String toString() {
		return JpaPipelineBeanUtil.doToString(this);
	}

	@Override
	public int hashCode() {
		return JpaPipelineBeanUtil.doToHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return JpaPipelineBeanUtil.doEquals(this, obj);
	}
}
