package org.infinitenature.cimonitor.adapter.rest.imp;

import org.infinitenature.cimonitor.adapter.rest.MonitorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class MonitorServiceImp implements MonitorService {
	private static final Logger LOGGER = LoggerFactory.getLogger(MonitorServiceImp.class);

	@Override
	public String startJob(int sourcePipelineId, int projectId, int pipelineId, int jobId) {
		System.out.println("started job");
		LOGGER.info("sourcePipelineId={}, projectId={}, pipelineId={}, jobId={}", sourcePipelineId, projectId,
				pipelineId, jobId);
		return "OK";
	}

}
