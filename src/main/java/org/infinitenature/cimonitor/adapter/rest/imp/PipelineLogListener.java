package org.infinitenature.cimonitor.adapter.rest.imp;

import org.gitlab4j.api.webhook.PipelineEvent;
import org.gitlab4j.api.webhook.PipelineEvent.Build;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class PipelineLogListener extends AbstractWebHookListener {
	private static final Logger LOGGER = LoggerFactory.getLogger(PipelineLogListener.class);

	@Override
	public void onPipelineEvent(PipelineEvent pipelineEvent) {
		LOGGER.info("onPipelineEvent");
		LOGGER.info("Pipline id: {}", pipelineEvent.getObjectAttributes().getId());
		LOGGER.info("Status: {}", pipelineEvent.getObjectAttributes().getStatus());
		LOGGER.info(pipelineEvent.getProject().getName());
		LOGGER.info("Project id: {}", pipelineEvent.getProject().getId());
		for (Build build : pipelineEvent.getBuilds()) {
			LOGGER.info("\tBuild id: {}", build.getId());
			LOGGER.info("\tBuild name: {}", build.getName());
			LOGGER.info("\tBuild stage: {}", build.getStage());
			LOGGER.info("\tBuild status: {}", build.getStatus());
		}
	}
}