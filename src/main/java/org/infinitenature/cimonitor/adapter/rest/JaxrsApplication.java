package org.infinitenature.cimonitor.adapter.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.springframework.stereotype.Component;

@Component
@ApplicationPath("/api/v1")
public class JaxrsApplication extends Application {

}
