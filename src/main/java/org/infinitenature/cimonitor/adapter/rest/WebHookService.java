package org.infinitenature.cimonitor.adapter.rest;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

@Path("hooks")
public interface WebHookService {

	@POST
	public Response consumeHook(@Context HttpServletRequest request);
}
