package org.infinitenature.cimonitor.adapter.rest.imp;

import java.util.List;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.webhook.WebHookListener;
import org.gitlab4j.api.webhook.WebHookManager;
import org.infinitenature.cimonitor.adapter.rest.WebHookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WebHookServiceImpl implements WebHookService {

	@Autowired
	private List<WebHookListener> webHookLisenters = new ArrayList<>();

	private WebHookManager manager = new WebHookManager();

	@PostConstruct
	public void init() {
		webHookLisenters.forEach(manager::addListener);
	}

	@Override
	public Response consumeHook(HttpServletRequest request) {
		try {
			manager.handleEvent(request);
		} catch (GitLabApiException e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		return Response.ok().build();
	}

}
