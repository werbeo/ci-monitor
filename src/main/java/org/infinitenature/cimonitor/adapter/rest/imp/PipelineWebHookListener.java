package org.infinitenature.cimonitor.adapter.rest.imp;

import org.gitlab4j.api.webhook.PipelineEvent;
import org.infinitenature.cimonitor.core.ports.PipelineCommands;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PipelineWebHookListener extends AbstractWebHookListener {
	@Autowired
	private PipelineMapper mapper;

	@Autowired
	private PipelineCommands pipelineCommands;

	@Override
	public void onPipelineEvent(PipelineEvent pipelineEvent) {
		pipelineCommands.createOrUpdatePipeline(mapper.map(pipelineEvent));
	}

}
