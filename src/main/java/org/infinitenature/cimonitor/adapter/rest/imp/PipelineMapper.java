package org.infinitenature.cimonitor.adapter.rest.imp;

import org.gitlab4j.api.webhook.PipelineEvent;
import org.infinitenature.cimonitor.core.model.Pipeline;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface PipelineMapper {
	@Mappings({ @Mapping(target = "beforeSha", source = "objectAttributes.beforeSha"),
			@Mapping(target = "createdAt", source = "objectAttributes.createdAt"),
			@Mapping(target = "duration", source = "objectAttributes.duration"),
			@Mapping(target = "finishedAt", source = "objectAttributes.finishedAt"),
			@Mapping(target = "id", source = "objectAttributes.id"),
			@Mapping(target = "ref", source = "objectAttributes.ref"),
			@Mapping(target = "sha", source = "objectAttributes.sha"),
			@Mapping(target = "stages", source = "objectAttributes.stages"),
			@Mapping(target = "status", source = "objectAttributes.status"),
			@Mapping(target = "tag", source = "objectAttributes.tag") })
	
	public Pipeline map(PipelineEvent pipelineEvent);
}
