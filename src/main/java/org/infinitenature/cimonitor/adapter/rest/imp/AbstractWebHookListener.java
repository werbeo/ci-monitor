package org.infinitenature.cimonitor.adapter.rest.imp;

import org.gitlab4j.api.webhook.BuildEvent;
import org.gitlab4j.api.webhook.IssueEvent;
import org.gitlab4j.api.webhook.MergeRequestEvent;
import org.gitlab4j.api.webhook.NoteEvent;
import org.gitlab4j.api.webhook.PipelineEvent;
import org.gitlab4j.api.webhook.PushEvent;
import org.gitlab4j.api.webhook.TagPushEvent;
import org.gitlab4j.api.webhook.WebHookListener;
import org.gitlab4j.api.webhook.WikiPageEvent;

public abstract class AbstractWebHookListener implements WebHookListener {

	@Override
	public void onBuildEvent(BuildEvent buildEvent) {
	}

	@Override
	public void onIssueEvent(IssueEvent event) {
	}

	@Override
	public void onMergeRequestEvent(MergeRequestEvent event) {
	}

	@Override
	public void onNoteEvent(NoteEvent noteEvent) {

	}

	@Override
	public void onPipelineEvent(PipelineEvent pipelineEvent) {
	}

	@Override
	public void onPushEvent(PushEvent pushEvent) {
	}

	@Override
	public void onTagPushEvent(TagPushEvent tagPushEvent) {
	}

	@Override
	public void onWikiPageEvent(WikiPageEvent wikiEvent) {
	}

}
