package org.infinitenature.cimonitor.adapter.rest;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("jobs")
public interface MonitorService {
	@POST
	@Path("/start/{sourcePipelineId}/{projectId}/{pipelineId}/{jobId}")
	public String startJob(@PathParam("sourcePipelineId") int sourcePipelineId, @PathParam("projectId") int projectId,
			@PathParam("pipelineId") int pipelineId, @PathParam("jobId") int jobId);
}
