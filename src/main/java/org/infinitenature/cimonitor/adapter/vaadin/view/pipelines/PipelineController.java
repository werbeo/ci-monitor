package org.infinitenature.cimonitor.adapter.vaadin.view.pipelines;

import java.util.stream.Stream;

import org.infinitenature.cimonitor.core.model.Pipeline;

public interface PipelineController {

	public Stream<Pipeline> fetchData(int offset, int limit);

	public int countData();
}
