package org.infinitenature.cimonitor.adapter.vaadin.view.pipelines;

import com.vaadin.ui.Composite;

public class PipelineViewImpl extends Composite implements PipelineView {

	private PipelineController pipelineController;

	public PipelineViewImpl() {
		super();
	}

	public PipelineViewImpl(PipelineController pipelineController) {
		super();
		this.pipelineController = pipelineController;
	}

	public void setPipelineController(PipelineController pipelineController) {
		this.pipelineController = pipelineController;
	}
}
