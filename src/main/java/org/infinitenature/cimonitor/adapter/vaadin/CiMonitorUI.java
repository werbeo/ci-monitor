package org.infinitenature.cimonitor.adapter.vaadin;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewDisplay;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.annotation.SpringViewDisplay;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;

@SpringUI
@SpringViewDisplay
public class CiMonitorUI extends UI implements ViewDisplay {

	private Panel viewDisplay;

	@Override
	protected void init(VaadinRequest request) {
		viewDisplay = new Panel();
		viewDisplay.setSizeFull();
		setContent(viewDisplay);
	}

	@Override
	public void showView(View view) {
		viewDisplay.setContent(view.getViewComponent());

	}

}
