package org.infinitenature.cimonitor.adapter.vaadin.view.pipelines;

import java.util.stream.Stream;

import org.infinitenature.cimonitor.core.model.Pipeline;

import com.vaadin.data.provider.AbstractBackEndDataProvider;
import com.vaadin.data.provider.Query;

public class PipelineDataProvider extends AbstractBackEndDataProvider<Pipeline, Void> {
	private PipelineController controller;

	@Override
	protected Stream<Pipeline> fetchFromBackEnd(Query<Pipeline, Void> query) {
		return controller.fetchData(query.getOffset(), query.getLimit());
	}

	@Override
	protected int sizeInBackEnd(Query<Pipeline, Void> query) {
		return controller.countData();
	}

}
