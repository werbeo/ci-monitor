package org.infinitenature.cimonitor.adapter.vaadin.view.pipelines;

import java.util.stream.Stream;

import org.infinitenature.cimonitor.core.model.Pipeline;
import org.infinitenature.cimonitor.core.ports.PipelineQueries;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Component;

@SpringView
public class PipelineControllerImpl implements PipelineController, View {
	private PipelineView pipeLineView = new PipelineViewImpl(this);
	@Autowired
	private PipelineQueries pipelineQueries;

	@Override
	public Component getViewComponent() {
		return pipeLineView;
	}

	@Override
	public Stream<Pipeline> fetchData(int offset, int limit) {
		return pipelineQueries.findPipelines(offset, limit);
	}

	@Override
	public int countData() {
		return pipelineQueries.count();
	}
}
